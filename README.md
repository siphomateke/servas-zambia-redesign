# Servas Zambia Website Redesign

Redesigned version of the Servas Zambia website created by Sipho Mateke.

## Usage

To view the final website, please open `./dist/index.html` and **NOT** `./index.html`.

## Building

Alternatively, to develop the website:

```bash
yarn install
yarn dev
```

To build for production:

```bash
yarn build
```
